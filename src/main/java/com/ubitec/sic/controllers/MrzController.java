package com.ubitec.sic.controllers;

import com.ubitec.sic.utils.Executors;
import com.ubitec.sic.services.FileStorageService;
import com.ubitec.sic.model.MrzResponse;
import com.ubitec.sic.model.UploadFileResponse;
import com.ubitec.sic.utils.FileUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/api/")
@Slf4j
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class MrzController {
	
	private static final String PYTHON_TESSERACT = "python D:/projects/mrz-reader/mrz-reader/reader.py ";
	
	private FileStorageService fileStorageService;
	
	@GetMapping("/check-python-version")
	public Object checkPythonVersion() {
		log.info("Exec call python --version");
		return Executors.executeScript("python --version");
	}
	
	@PostMapping(value = "/uploadFileAndGetDownloadLink", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public UploadFileResponse uploadFileAndGetDownloadLink(@RequestParam("file") MultipartFile file) {
		log.info("Triggered Upload file API");
		String fileName = fileStorageService.storeFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
				                         .path("/downloadFile/")
				                         .path(fileName)
				                         .toUriString();
		log.info("Uploaded Successful");
		return UploadFileResponse.builder()
				       .fileName(fileName)
				       .fileDownloadUri(fileDownloadUri)
				       .fileType(file.getContentType())
				       .size(file.getSize())
				       .build();
	}
	
	@PostMapping(value = "/get-mrz", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public MrzResponse uploadFile(@RequestParam("file") MultipartFile uploadedFile) throws IOException {
		File file = FileUtils.convert(uploadedFile);
		log.info("Started Exec Python Code");
		String command = PYTHON_TESSERACT + FileUtils.cleanPath(file.getAbsolutePath());
		log.info("Finished Exec Python Code");
		if (file.delete()) {
			log.info("uploaded file deleted");
		}
		return MrzResponse.builder().mrz(Executors.executeScript(command)).build();
	}
	
	@PostMapping(value = "/get-mrz-base-64")
	public MrzResponse uploadFile(@RequestParam("file") String base64String) throws IOException {
//		File file = FileUtils.convert(uploadedFile);
//		log.info("Started Exec Python Code");
//		String command = PYTHON_TESSERACT + UPLOAD_DIR + fileName;
//		log.info("Finished Exec Python Code");
//		return MrzResponse.builder().mrz(Executors.executeScript(command)).build();
		return MrzResponse.builder().build();
	}
}
