package com.ubitec.sic;

import com.ubitec.sic.configuration.WebConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({FileStorageProperties.class})
public class MrzReaderApplication {
	public static void main(String[] args) {
		SpringApplication.run(MrzReaderApplication.class, args);
	}
}
