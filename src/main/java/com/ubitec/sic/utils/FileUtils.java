package com.ubitec.sic.utils;

import com.ubitec.sic.exceptions.MyFileNotFoundException;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

@UtilityClass
@Slf4j
public class FileUtils {
	public File convert(MultipartFile file) throws IOException {
		if(Objects.nonNull(file) && Objects.nonNull(file.getOriginalFilename())) {
			String fileName = StringUtils.join("-", Dates.now(), file.getOriginalFilename());
			File convertedFile = new File(fileName);
			try (InputStream is = file.getInputStream()) {
				Files.copy(is, convertedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
			return convertedFile;
		}
		throw new MyFileNotFoundException("Cannot find file");
	}
	
	public File convert(String base64) throws IOException {
		return null;
	}
	
	public String cleanPath(String path) {
		if(StringUtils.isEmpty(path)) {
			return StringUtils.EMPTY;
		}
		return StringUtils.replace(path, "\\", "/");
	}
}
