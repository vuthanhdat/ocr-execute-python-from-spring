package com.ubitec.sic.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@UtilityClass
@Slf4j
public class Executors {
	public String executeScript(String script) {
		CommandLine commandLine = CommandLine.parse(script);
		DefaultExecutor defaultExecutor = new DefaultExecutor();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
		defaultExecutor.setStreamHandler(streamHandler);
		try {
			defaultExecutor.execute(commandLine);
		} catch (IOException ex) {
			log.error("Cannot Execute script ---\n {} \n with Stacktrace {} \n", script, outputStream.toString());
		}
		return outputStream.toString();
	}
}
